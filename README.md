# What is factorio-updater?
### Why "factorio-updater"?
Welp I really need to find a name which will not be taken down eventually, as the current one may imply that I'm affiliated with Factorio or Wube Software, which is clearly not the case as, let's face it, I'm in no way competent enough. If you have one, you can submit an issue on this repo, or contact me directly on discord "Monique#6823" or [Twitter](https://www.twitter.com/lazy_monique)

### Why do this when some tools already exist?
I hate python. I mean, I've used python a lot, but the fact that the user needs to download things via pip, the fact that python3 and python 2.7 are horrible to deal with, and the fact that I broke pip and pip3 on my server beyond repair, all this contributes to me not wanting nor having the possibility to use such tools without having to reinstall a fresh debian to use this. And I'm way to lazy to take 1 hour to reinstall Debian and all software on it, so why not just take more than ~~30~~ 180 hours to make a tool which will save me this precious time?

### What does it actually do?
Currently it can:
* Install servers
* Update servers (Is now totally safe for your maps!)
* Mod installation/update (Should now work!)
* Mod installation from a mod-list.json (Tested once and it worked so it's good enough for me)

What is also planned:
* Guided server configuration

# How does it work?
### What do I need to use it?
Nothing. You just need the binary, and it **should** just work

### How do I use it?
You have to download the binary [from here](https://gitlab.com/la-livre/factorio-updater/raw/master/bin/factorio-updater?inline=false) and launch it or compile it yourself. Instructions are provided with the --help flag. Always use the -x flag when dealing with experimental versions!

Basically, the default folders it will touch are $HOME/factorio and $HOME/factorio-updater, but this is configurable easily using the flags! To set a string flag, you have to use it like "--install-mod=rso-mod"

# Can I contribute?
Please feel free to get in touch with me!
