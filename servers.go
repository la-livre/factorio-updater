package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

func downloadUpdate(from serverVersionStruct, to serverVersionStruct, auth authStruct) error {
	var links []string
	url := fmt.Sprintf("https://updater.factorio.com/get-download-link?from=%s&to=%s&username=%s&token=%s&package=core-linux_headless64", from.String(), to.String(), auth.Username, auth.Token)
	req, err := http.Get(url)
	if err != nil {
		return err
	}
	defer req.Body.Close()
	linksBytes, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return err
	}
	if err = json.Unmarshal(linksBytes, &links); err != nil {
		return err
	}
	head, err := http.Head(links[0])
	if err != nil {
		return err
	}
	size, err := strconv.Atoi(head.Header.Get("content-length"))
	if err != nil {
		return err
	}
	stat, err := os.Stat(INSTALLPATH + "/updates/" + from.String() + "-" + to.String() + ".zip")
	if err == nil {
		if int64(size) == stat.Size() {
			fmt.Println(INSTALLPATH + "/updates/" + from.String() + "-" + to.String() + ".zip already exists and has the same size than factorio.com's one. If you really want to redownload it, please delete it from said path first")
			return nil
		}
	}
	out, err := os.Create(INSTALLPATH + "/updates/" + from.String() + "-" + to.String() + ".zip")
	if err != nil {
		return err
	}
	defer out.Close()
	if err = downloadFile(links[0], out); err != nil {
		return err
	}
	return nil
}

func downloadServer(version serverVersionStruct) error {
	head, err := http.Head("https://www.factorio.com/get-download/" + version.String() + "/headless/linux64")
	if err != nil {
		return err
	}
	size, err := strconv.Atoi(head.Header.Get("content-length"))
	if err != nil {
		return err
	}
	stat, err := os.Stat(INSTALLPATH + "/servers/" + version.String() + ".tar.gz")
	if err == nil {
		if int64(size) == stat.Size() {
			fmt.Println(INSTALLPATH + "/servers/" + version.String() + ".tar.gz already exists and has the same size than factorio.com's one. If you really want to redownload it, please delete it from said path first")
			return nil
		}
	}
	out, err := os.Create(INSTALLPATH + "/servers/" + version.String() + ".tar.gz")
	if err != nil {
		return err
	}
	defer out.Close()
	if err = downloadFile("https://www.factorio.com/get-download/"+version.String()+"/headless/linux64", out); err != nil {
		return err
	}
	return nil
}

func checkLatest(experimental bool) (current serverVersionStruct, latest serverVersionStruct, err error) {
	current, err = getServerVersion()
	if err != nil {
		return current, latest, err
	}
	latest, err = getServerLatest(current.Major, current.Minor, experimental)
	if err != nil {
		return current, latest, err
	}
	return current, latest, nil
}

func getLatest(experimental bool) (serverVersionStruct, error) {
	var (
		version serverVersionStruct
		err     error
		doc     *goquery.Document
		url     string
	)
	if experimental {
		url = "https://www.factorio.com/download-headless/experimental"
	} else {
		url = "https://www.factorio.com/download-headless"
	}
	doc, err = goquery.NewDocument(url)
	if err != nil {
		return version, err
	}
	link, _ := doc.Find("body>div>ul a").First().Attr("href")
	version, err = parseVersion(strings.Split(link, "/")[2])
	if err != nil {
		return version, err
	}
	return version, nil
}

func getServerLatest(major int, minor int, experimental bool) (serverVersionStruct, error) {
	var version serverVersionStruct
	var done bool
	var errLoop error
	var url string
	if experimental {
		url = "https://www.factorio.com/download-headless/experimental"
	} else {
		url = "https://www.factorio.com/download-headless"
	}
	doc, err := goquery.NewDocument(url)
	if err != nil {
		return version, err
	}
	doc.Find("body>div>ul a").Each(func(i int, sel *goquery.Selection) {
		if !done {
			link, _ := sel.Attr("href")
			ver := strings.Split(link, "/")[2]
			tempVer, err := parseVersion(ver)
			if err != nil {
				errLoop = err
				done = true
			}
			if major == tempVer.Major && minor == tempVer.Minor {
				version.Major = tempVer.Major
				version.Minor = tempVer.Minor
				version.Fix = tempVer.Fix
				done = true
			}
		}
	})
	if errLoop != nil {
		return version, err
	}
	if done {
		return version, nil
	}
	return version, errors.New("no latest version found")
}

func getServerVersion() (version serverVersionStruct, err error) {
	stat, err := os.Stat(BASEPATH + "/bin/x64/factorio")
	if err != nil {
		return version, err
	}
	if stat.IsDir() {
		return version, errors.New(BASEPATH + "/bin/x64/factorio is a directory but should be an executable file. Factorio instance seems to be corrupted")
	}
	cmd := exec.Command(BASEPATH+"/bin/x64/factorio", "--version")
	out, err := cmd.CombinedOutput()
	if err != nil {
		return version, err
	}
	ver := strings.Split(string(out), " ")[1]
	version, err = parseVersion(ver)
	if err != nil {
		return version, err
	}
	return version, nil
}
