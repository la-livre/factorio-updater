module factorio-updater

go 1.14

require (
	code.cloudfoundry.org/bytefmt v0.0.0-20200131002437-cf55d5288a48
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/dsnet/compress v0.0.1 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/mholt/archiver v3.1.1+incompatible
	github.com/nwaples/rardecode v1.1.0 // indirect
	github.com/ogier/pflag v0.0.1
	github.com/otiai10/copy v1.1.1
	github.com/pierrec/lz4 v2.5.2+incompatible // indirect
	github.com/ulikunitz/xz v0.5.7 // indirect
	github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8 // indirect
)
