package main

import (
	"fmt"
	"os"

	"github.com/ogier/pflag"
)

// BASEPATH will be used to determine which path to write data to
var (
	BASEPATH    string
	INSTALLPATH string
)

func main() {
	var err error
	var auth authStruct
	path := pflag.String("path", "", "Defines the path of the current factorio headless instance, or the one to be create (eg. path to a folder with subdirectories \"bin\" and \"data\"). The default one is $HOME/factorio")
	installPath := pflag.String("install-path", "", "Defines this tool's installation folder path (eg. path to a folder with subdirectories \"factorio/mods\", \"factorio/updates\" and \"factorio/servers\"). The default one is $HOME/factorio-updater")

	install := pflag.Bool("install", false, "When flag is set, will install latest factorio version in the directory pointed by install-path. install-path will have to be empty or non-existing")
	update := pflag.Bool("update", false, "When flag is set, will update the factorio instance pointed by install-path")
	experimental := pflag.BoolP("experimental", "x", false, "When flag is set, latest version will be the experimental one")

	installMod := pflag.String("install-mod", "", "When flag is set, will attempt to install the mod. The name has to the the one in the URL of the mod portal. \"Resource Spawner Overhaul\" will be \"rso-mod\"")
	installMods := pflag.String("install-mods", "", "When flag is set, will attempt to install all the mods enabled in a provided mods-list.json created by the game.")
	updateMods := pflag.Bool("update-mods", false, "When flag is set, will attempt to update the server's mods")

	username := pflag.String("username", "", "Your factorio username. Needed to download mods")
	password := pflag.String("password", "", "Your factorio password. Needed to download mods. You should pass your authentification token instead with the --token flag.")
	token := pflag.String("token", "", "Your factorio token. Needed to download mods.")
	pflag.Parse()
	setPath(path, installPath, install)
	if *installMod == "" && !*updateMods && !*update && !*install && *installMods == "" {
		fmt.Println("No argument provided. You should probably used the -h flag to display help")
		os.Exit(-1)
	}
	if *installMod == "" && !*updateMods && !*update && *installMods == "" {
		fmt.Println("No authentification has to be made")
	} else if *username == "" || (*token == "" && *password == "") {
		panic("Authentification has to be made but you didn't populate the flags --username and --token or --password")
	} else {
		fmt.Println("Authentification in progress...")
		if *token == "" {
			auth, err = getToken(*username, *password)
			if err != nil {
				panic(err)
			}
		} else {
			auth, err = checkToken(*username, *token)
			if err != nil {
				panic(err)
			}
		}
		fmt.Printf("Authentification done! Token is: %s\n", auth.Token)
	}
	if *install {
		installRoutine(*experimental)
	}
	if *update {
		updateRoutine(*experimental, auth)
	}
	if *installMod != "" {
		installModRoutine(*installMod, auth)
	}
	if *installMods != "" {
		installModsRoutine(*installMods, auth)
	}
	if *updateMods {
		updateModsRoutine(auth)
	}
	return
}
