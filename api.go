package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"

	"code.cloudfoundry.org/bytefmt"
)

type authStruct struct {
	Username string
	Password string
	Token    string
}

func downloadFile(url string, file *os.File) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode == 401 {
		return errors.New("bad authentification")
	}
	if resp.StatusCode == 404 {
		return errors.New("file not found")
	}
	size, err := strconv.Atoi(resp.Header.Get("content-length"))
	if err != nil {
		return err
	}
	fmt.Printf("Data to download: %s\n", bytefmt.ByteSize(uint64(size)))
	if _, err := io.Copy(file, resp.Body); err != nil {
		return err
	}
	return nil
}

func getToken(username string, password string) (authStruct, error) {
	var auth authStruct
	var t []string
	auth.Username = username
	auth.Password = password
	resp := struct {
		Message string `json:"message"`
		Status  int    `json:"status"`
	}{}
	r, err := http.Post("https://auth.factorio.com/api-login?username="+username+"&password="+password, "application/x-www-form-urlencoded", nil)
	if err != nil {
		return auth, err
	}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return auth, err
	}
	if r.StatusCode == 401 {
		if err := json.Unmarshal(body, &resp); err != nil {
			panic(err)
		}
		return auth, errors.New(resp.Message)
	}
	defer r.Body.Close()
	if err := json.Unmarshal(body, &t); err != nil {
		panic(err)
	}
	auth.Token = t[0]
	return auth, nil
}

func checkToken(username string, token string) (authStruct, error) {
	var auth authStruct
	auth.Username = username
	auth.Token = token
	r, err := http.Head("https://mods.factorio.com/download/rso-mod/5a5f1ae6adcc441024d73a0d?username=" + username + "&token=" + token)
	if err != nil {
		return auth, err
	}
	if r.StatusCode == 403 {
		panic("Authentification with provided token seems incorrect")
	}
	return auth, nil
}
