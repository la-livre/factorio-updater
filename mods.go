package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
)

func downloadMod(name string, auth authStruct, major int, minor int) (modRelease, error) {
	release, err := pickLatestMod(name, major, minor)
	if err != nil {
		return modRelease{}, err
	}
	resp, err := http.Head("https://mods.factorio.com" + release.DownloadURL + "?username=" + auth.Username + "&token=" + auth.Token)
	if err != nil {
		return modRelease{}, err
	}
	defer resp.Body.Close()
	size, err := strconv.Atoi(resp.Header.Get("content-length"))
	if err != nil {
		return modRelease{}, err
	}
	stat, err := os.Stat(INSTALLPATH + "/mods/" + release.Filename)
	if err == nil {
		if int64(size) == stat.Size() {
			fmt.Println(INSTALLPATH + "/mods/" + release.Filename + " already exists and has the same size than factorio.com's one. If you really want to redownload it, please delete it from said path first")
			return release, nil
		}
	}
	out, err := os.Create(INSTALLPATH + "/mods/" + release.Filename)
	if err != nil {
		return modRelease{}, err
	}
	defer out.Close()
	if err = downloadFile("https://mods.factorio.com"+release.DownloadURL+"?username="+auth.Username+"&token="+auth.Token, out); err != nil {
		return modRelease{}, err
	}
	return release, nil
}

func pickLatestMod(name string, major int, minor int) (modRelease, error) {
	var m mod
	var version serverVersionStruct
	releaseIndex := -1
	var rel modRelease
	version.Major = major
	version.Minor = minor
	resp, err := http.Get("https://mods.factorio.com/api/mods/" + name + "/full")
	if err != nil {
		return rel, err
	}
	if resp.StatusCode == 404 {
		return rel, errors.New("mod not found")
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return rel, err
	}
	if err := json.Unmarshal(body, &m); err != nil {
		return rel, err
	}
	for i, release := range m.Releases {
		modVersion, err := parseVersion(release.InfoJSON.FactorioVersion)
		if err != nil {
			return rel, err
		}
		if modVersion.isCompatible(version) {
			releaseIndex = i
		}
	}
	if releaseIndex == -1 {
		return rel, errors.New("this mod is not compatible with provided game version")
	}
	return m.Releases[releaseIndex], nil
}

type mod struct {
	CreatedAt       string         `json:"created_at"`
	Description     string         `json:"description"`
	DescriptionHTML string         `json:"description_html"`
	DownloadsCount  int            `json:"downloads_count"`
	FirstFileMedia  modMediaType   `json:"first_file_media"`
	GameVersions    []string       `json:"game_versions"`
	GithubPath      string         `json:"github_path"`
	Homepage        string         `json:"homepage"`
	ID              int            `json:"id"`
	LatestRelease   modRelease     `json:"latest_release"`
	LicenseName     string         `json:"license_name"`
	LicenseURL      string         `json:"license_url"`
	MediaFiles      []modMediaType `json:"media_files"`
	Name            string         `json:"name"`
	Owner           string         `json:"owner"`
	Releases        []modRelease   `json:"releases"`
	Summary         string         `json:"summary"`
	Tags            []modTag       `json:"tags"`
	Title           string         `json:"title"`
	UpdatedAt       string         `json:"updated_at"`
	VisitsCount     int            `json:"visits_count"`
}

type modRelease struct {
	DownloadURL    string  `json:"download_url"`
	DownloadsCount int     `json:"downloads_count"`
	Filename       string  `json:"file_name"`
	InfoJSON       modInfo `json:"info_json"`
	Filesize       int     `json:"file_size"`
	GameVersion    string  `json:"game_version"` //Useless
	ID             int     `json:"id"`
	ReleasedAt     string  `json:"released_at"`
	Version        string  `json:"version"` // Looks like it's actually not there
}

type modInfo struct {
	Name            string `json:"name"`
	Title           string `json:"title"`
	FactorioVersion string `json:"factorio_version"` // Look, I'm unmarshalling an API response, I'm not responsible for all weird things I receive
}

type modMediaType struct {
	ID     int         `json:"id"`
	Width  int         `json:"width"`
	Height int         `json:"height"`
	Size   int         `json:"size"`
	URLS   modURLMedia `json:"urls"`
}

type modURLMedia struct {
	Original string `json:"original"`
	Thumb    string `json:"thumb"`
}

// A tag seems to be actually quite complex
type modTag struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Type        string `json:"type"`
}
