package main

import (
	"strconv"
	"strings"
)

type serverVersionStruct struct {
	Major int
	Minor int
	Fix   int
}

func (ver serverVersionStruct) isEqual(version serverVersionStruct) bool {
	if ver.Major == version.Major && ver.Minor == version.Minor && ver.Fix == version.Fix {
		return true
	}
	return false
}

func (ver serverVersionStruct) isOutdatedBy(version serverVersionStruct) bool {
	if ver.Major == version.Major && ver.Minor == version.Minor && ver.Fix < version.Fix {
		return true
	}
	return false
}

func (ver serverVersionStruct) isCompatible(version serverVersionStruct) bool {
	if ver.Major == version.Major && ver.Minor == version.Minor {
		return true
	}
	return false
}

func (ver serverVersionStruct) String() string {
	return strconv.Itoa(ver.Major) + "." + strconv.Itoa(ver.Minor) + "." + strconv.Itoa(ver.Fix)
}

func parseVersion(str string) (serverVersionStruct, error) {
	var version serverVersionStruct
	var err error
	numbers := strings.Split(str, ".")
	version.Major, err = strconv.Atoi(numbers[0])
	if err != nil {
		return version, err
	}
	version.Minor, err = strconv.Atoi(numbers[1])
	if err != nil {
		return version, err
	}
	if len(numbers) == 2 {
		version.Fix = 0
		return version, nil
	}
	version.Fix, err = strconv.Atoi(numbers[2])
	if err != nil {
		return version, err
	}
	return version, nil
}
