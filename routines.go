package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/mholt/archiver"
	"github.com/otiai10/copy"
)

// MODS INSTALLATION FROM JSON ROUTINE

type modList struct {
	Mods []struct {
		Enabled bool   `json:"enabled"`
		Name    string `json:"name"`
	} `json:"mods"`
}

func installModsRoutine(jsonPath string, auth authStruct) {
	serverVersion, err := getServerVersion()
	if err != nil {
		panic(err)
	}
	var modL modList
	var mods []string
	fmt.Print("Verifying existence of the mod directory...")
	stat, err := os.Stat(BASEPATH + "/mods")
	if err != nil && os.IsNotExist(err) {
		if err = os.Mkdir(BASEPATH+"/mods", 0755); err != nil {
			panic(err)
		}
		stat, err = os.Stat(BASEPATH + "/mods")
		if err != nil {
			panic(err)
		}
	} else if err != nil {
		panic(err)
	}
	if !stat.IsDir() {
		panic(BASEPATH + "/mods" + "is not a folder. Perhaps this tool's installation directory is corrupted or you just put a wrong path")
	}
	fmt.Println(" Done\nListing all mods:")
	files, err := ioutil.ReadFile(jsonPath)
	if err != nil {
		panic(err)
	}
	if err = json.Unmarshal(files, &modL); err != nil {
		panic(err)
	}

	for _, file := range modL.Mods {
		if file.Enabled && file.Name != "base" {
			mods = append(mods, file.Name)
			fmt.Println("\t" + file.Name)
		}
	}

	for _, m := range mods {
		fmt.Println("Downloading: " + m)
		release, err := downloadMod(m, auth, serverVersion.Major, serverVersion.Minor)
		if err != nil && err.Error() != "mod not found" {
			panic(err)
		} else if err == nil {
			fmt.Println("Installing: " + m)
			modBytes, err := ioutil.ReadFile(INSTALLPATH + "/mods/" + release.Filename)
			if err != nil {
				panic(err)
			}
			if err = ioutil.WriteFile(BASEPATH+"/mods/"+release.Filename, modBytes, 0644); err != nil {
				panic(err)
			}
		} else {
			fmt.Printf("Mod \"%s\" not found. Ignoring.", m)
		}
	}
	fmt.Println("All mods should be installed!")
	os.Exit(0)
	return
}

// MODS UPDATE ROUTINE

func updateModsRoutine(auth authStruct) {
	serverVersion, err := getServerVersion()
	if err != nil {
		panic(err)
	}
	fmt.Print("Verifying existence of the mod directory...")
	stat, err := os.Stat(BASEPATH + "/mods")
	if err != nil && os.IsNotExist(err) {
		panic("This instance doesn't have a mod folder, no mods to update")
	} else if err != nil {
		panic(err)
	}
	if !stat.IsDir() {
		panic(BASEPATH + "/mods" + "is not a folder. Perhaps this tool's installation directory is corrupted or you just put a wrong path")
	}
	fmt.Println(" Done\nListing all mods:")

	files, err := ioutil.ReadFile(BASEPATH + "/mods/mod-list.json")
	if err != nil {
		panic(err)
	}
	modFiles, err := ioutil.ReadDir(BASEPATH + "/mods")
	if err != nil {
		panic(err)
	}
	modsMap := make(map[string]string)
	for _, f := range modFiles {
		if strings.HasSuffix(f.Name(), ".zip") {
			zip := archiver.Zip{ImplicitTopLevelFolder: true, OverwriteExisting: true}
			if err = zip.Extract(
				BASEPATH+"/mods/"+f.Name(),
				strings.TrimSuffix(f.Name(), ".zip")+"/info.json",
				BASEPATH+"/mods/"+"info.json"); err != nil {
				fmt.Println("Couldn't extract mod infos from " + f.Name() + ". Ignoring.")
			}
			b, err := ioutil.ReadFile(BASEPATH + "/mods/info.json/" + strings.TrimSuffix(f.Name(), ".zip") + "/info.json")
			if err != nil {
				fmt.Println("Couldn't extract mod infos from " + f.Name() + ". Ignoring.")
			}
			var infos modInfo
			if err = json.Unmarshal(b, &infos); err != nil {
				fmt.Println("Couldn't extract mod infos from " + f.Name() + ". Ignoring. This might be info.json inside the modfile being malformed. Please check it manually and report it to the mod author.")
			}
			modsMap[infos.Name] = f.Name()
		}
	}
	var mods []string
	var modL modList
	if err = json.Unmarshal(files, &modL); err != nil {
		panic(err)
	}
	for _, file := range modL.Mods {
		if file.Enabled && file.Name != "base" {
			mods = append(mods, file.Name)
			fmt.Println("\t" + file.Name)
		}
	}
	for _, m := range mods {
		fmt.Println("Downloading: " + m)
		release, err := downloadMod(m, auth, serverVersion.Major, serverVersion.Minor)
		if err != nil && err.Error() != "mod not found" {
			panic(err)
		} else if err == nil {
			fmt.Println("Installing: " + m)
			modBytes, err := ioutil.ReadFile(INSTALLPATH + "/mods/" + release.Filename)
			if err != nil {
				panic(err)
			}
			if err = os.Remove(BASEPATH + "/mods/" + modsMap[m]); err != nil {
				fmt.Println("Couldn't remove previous version of mod. Perhaps it was already removed. Please check it, as having several versions of the same mod will prevent the server from starting.")
			}
			if err = ioutil.WriteFile(BASEPATH+"/mods/"+release.Filename, modBytes, 0644); err != nil {
				panic(err)
			}
		} else {
			fmt.Printf("Mod \"%s\" not found. Ignoring.", m)
		}
	}
	fmt.Println("All mods should be up to date!")
	return
}

// MOD INSTALLATION ROUTINE

func installModRoutine(modName string, auth authStruct) {
	serverVersion, err := getServerVersion()
	if err != nil {
		panic(err)
	}

	fmt.Println("Downloading " + modName)
	release, err := downloadMod(modName, auth, serverVersion.Major, serverVersion.Minor)
	if err != nil {
		panic(err)
	}

	modFile, err := os.Stat(INSTALLPATH + "/mods/" + release.Filename)
	if err != nil {
		panic(err)
	}
	modSize := modFile.Size()
	stat, err := os.Stat(BASEPATH + "/mods")
	if err != nil && os.IsNotExist(err) {
		if err = os.Mkdir(BASEPATH+"/mods", 0755); err != nil {
			panic(err)
		}
		stat, err = os.Stat(BASEPATH + "/mods")
		if err != nil {
			panic(err)
		}
	} else if err != nil {
		panic(err)
	}
	if !stat.IsDir() {
		panic(BASEPATH + "/mods" + "is not a folder. Perhaps this tool's installation directory is corrupted or you just put a wrong path")
	}
	files, err := ioutil.ReadDir(BASEPATH + "/mods")
	if err != nil {
		panic(err)
	}
	for _, file := range files {
		name := file.Name()
		if name == release.Filename && file.Size() == modSize {
			fmt.Println(BASEPATH + "/mods/" + release.Filename + " already exists and has the same size than the one we just downloaded. If you really want to redownload it, please delete it from said path first")
			return
		}
		if name == release.Filename {
			fmt.Println("Mod is already installed but is likely to be from another version, possibly (probably) older. You should use the update-mods flag if you want to update")
			return
		}
	}
	if err = copy.Copy(INSTALLPATH+"/mods/"+release.Filename, BASEPATH+"/mods/"+release.Filename); err != nil {
		panic(err)
	}
	fmt.Println("Mod should have been successfully installed!")
	return
}

// UPDATE ROUTINE
var ErrNoUpdate = errors.New("no update package available")

type updateStruct struct {
	Package []struct {
		From         string `json:"from"`
		To           string `json:"to"`
		Stable       string `json:"stable"`
		Experimental string `json:"experimental"`
	} `json:"core-linux_headless64"`
}

func updateRoutine(experimental bool, auth authStruct) {
	var updates updateStruct
	version, err := getServerVersion()
	if err != nil {
		panic(err)
	}
	fmt.Println("Current version is: " + version.String() + ". Downloading list of available updates...")
	resp, err := http.Get("https://updater.factorio.com/get-available-versions")
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	jsonBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Println("List downloaded. Reading list...")
	json.Unmarshal(jsonBytes, &updates)
	fmt.Println("Looking for available updates...")
	target, err := getUpdateTarget(updates, version, experimental)
	if err != nil {
		panic(err)
	}
	if target.isEqual(version) {
		fmt.Println("No updates were found. Assuming server is up to date!")
		os.Exit(0)
		return
	}
	fmt.Println("Server should update to " + target.String() + ". Downloading all of them.")
	for version.Fix < target.Fix {
		to, err := getNextUpdate(updates, version)
		if err != nil {
			if errors.Is(err, ErrNoUpdate) {
				fmt.Println("No update package available right now. Please retry later when factorio devs release the update package.")
				os.Exit(1)
			}
			panic(err)
		}
		fmt.Printf("Downloading update from %s to %s...\n", version.String(), to.String())
		if err = downloadUpdate(version, to, auth); err != nil {
			panic("Error downloading update.\n" + err.Error())
		}
		fmt.Print("Applying update...")
		updatepath := INSTALLPATH + "/updates/" + version.String() + "-" + to.String() + ".zip"
		cmd := exec.Command(BASEPATH+"/bin/x64/factorio", "--apply-update="+updatepath)
		_, err = cmd.CombinedOutput()
		if err != nil {
			panic(err)
		}
		fmt.Println(" Done")
		version.Fix = to.Fix
	}
	fmt.Println("Server should now be up to date!")
	os.Exit(0)
}

func getNextUpdate(updates updateStruct, from serverVersionStruct) (serverVersionStruct, error) {
	var version serverVersionStruct
	for _, up := range updates.Package {
		var temp serverVersionStruct
		temp, err := parseVersion(up.From)
		if err != nil {
			return serverVersionStruct{}, nil
		}
		if from.isCompatible(temp) && temp.Fix == from.Fix {
			return parseVersion(up.To)
		}
	}
	return version, ErrNoUpdate
}

func getUpdateTarget(updates updateStruct, version serverVersionStruct, experimental bool) (serverVersionStruct, error) {
	var err error
	for _, up := range updates.Package {
		var tempV serverVersionStruct
		if up.Stable != "" && !experimental {
			tempV, err = parseVersion(up.Stable)
			if err != nil {
				return version, err
			}
			if tempV.isOutdatedBy(tempV) {
				version.Fix = tempV.Fix
				return version, nil
			}
		} else if up.Experimental != "" && experimental {
			tempV, err = parseVersion(up.Experimental)
			if err != nil {
				return version, err
			}
			if tempV.isOutdatedBy(tempV) {
				version.Fix = tempV.Fix
				return version, nil
			}
		}
	} // At this point, only ancient versions will remain

	for _, up := range updates.Package {
		var tempV serverVersionStruct
		if up.Experimental == "" && up.Stable == "" {
			tempV, err = parseVersion(up.To)
			if err != nil {
				return version, err
			}
			if version.isOutdatedBy(tempV) {
				version.Fix = tempV.Fix
			}
		}
	}
	return version, nil
}

// INSTALLATION ROUTINE

func installRoutine(experimental bool) {
	fmt.Printf("You chose to: Install factorio in %s\n", BASEPATH)
	if experimental {
		fmt.Println("Attempting to determine latest experimental version...")
	} else {
		fmt.Println("Attempting to determine latest version...")
	}
	latest, err := getLatest(experimental)
	if err != nil {
		panic(err)
	}
	if experimental {
		fmt.Printf("Latest experimental version is %s\n", latest.String())
	} else {
		fmt.Printf("Latest version is %s\n", latest.String())
	}
	if err = downloadServer(latest); err != nil {
		panic(err)
	}
	fmt.Printf("Decompressing server files to %s... (this can take a minute or two.)\n", BASEPATH)
	if err = archiver.DefaultTarXz.Unarchive(INSTALLPATH+"/servers/"+latest.String()+".tar.gz", BASEPATH); err != nil {
		panic(err)
	}
	fmt.Println("Installation finished!")
	os.Exit(0)
	return
}

// PATH ROUTINE

func setPath(path *string, installPath *string, install *bool) {
	var err error
	if *path == "" {
		BASEPATH = os.Getenv("HOME")
		BASEPATH, err = filepath.Abs(BASEPATH + "/factorio")
		if err != nil {
			panic(err)
		}
	} else {
		BASEPATH, err = filepath.Abs(*path)
		if err != nil {
			panic(err)
		}
	}
	if !*install {
		BASEPATH = BASEPATH + "/factorio"
	}
	if *installPath == "" {
		INSTALLPATH = os.Getenv("HOME")
		INSTALLPATH, err = filepath.Abs(INSTALLPATH + "/factorio-updater")
		if err != nil {
			panic(err)
		}

	} else {
		INSTALLPATH, err = filepath.Abs(*installPath)
		if err != nil {
			panic(err)
		}
	}
	stat, err := os.Stat(INSTALLPATH + "/mods")
	if err != nil && os.IsNotExist(err) {
		if err = os.MkdirAll(INSTALLPATH+"/mods", 0755); err != nil {
			panic(err)
		}
		stat, err = os.Stat(INSTALLPATH + "/mods")
		if err != nil {
			panic(err)
		}
	} else if err != nil {
		panic(err)
	}
	if !stat.IsDir() {
		panic(INSTALLPATH + "/mods" + "is not a folder. Perhaps this tool's installation directory is corrupted or you just put a wrong path")
	}
	stat, err = os.Stat(INSTALLPATH + "/servers")
	if err != nil && os.IsNotExist(err) {
		if err = os.MkdirAll(INSTALLPATH+"/servers", 0755); err != nil {
			panic(err)
		}
		stat, err = os.Stat(INSTALLPATH + "/servers")
		if err != nil {
			panic(err)
		}
	} else if err != nil {
		panic(err)
	}
	if !stat.IsDir() {
		panic(INSTALLPATH + "/servers" + "is not a folder. Perhaps this tool's installation directory is corrupted or you just put a wrong path")
	}
	stat, err = os.Stat(INSTALLPATH + "/updates")
	if err != nil && os.IsNotExist(err) {
		if err = os.MkdirAll(INSTALLPATH+"/updates", 0755); err != nil {
			panic(err)
		}
		stat, err = os.Stat(INSTALLPATH + "/updates")
		if err != nil {
			panic(err)
		}
	} else if err != nil {
		panic(err)
	}
	if !stat.IsDir() {
		panic(INSTALLPATH + "/updates" + "is not a folder. Perhaps this tool's installation directory is corrupted or you just put a wrong path")
	}
	stat, err = os.Stat(BASEPATH)
	if err != nil && os.IsNotExist(err) {
		if err = os.MkdirAll(BASEPATH, 0755); err != nil {
			panic(err)
		}
		stat, err = os.Stat(BASEPATH)
		if err != nil {
			panic(err)
		}
	} else if err != nil {
		panic(err)
	}
	if !stat.IsDir() {
		panic(BASEPATH + "is not a folder. Perhaps this tool's installation directory is corrupted or you just put a wrong path")
	}
}
